import * as actionType from "../actions/actionTypes";
import { toast } from "react-toastify";

const initialState = {
  graphData: {
    nodes: [],
    links: []
  },
  loading: false,
  searchedNode: [],
  centeredSearchedNode: false,
  ApiFlag: "",
  showSideBar: false,
  tags: [],
  selectedTargetNodes: [],
  selectedSourceNodes: [],
  modalOpened: false,
  clickedNodeDetails: {},
  dataLoaded: false,
  ref: {},
  highlightLink: [],
  highlightNodes: [],
  onNodeClickToLoad: false,
  isZoomToLoaded: true,
  loadedGlobalAlphaLevel: [],
  width: window.innerWidth,
  height: window.innerHeight
};

const loaderStart = (state, action) => {
  return {
    ...state,
    loading: true
  };
};

const loaderStop = (state, action) => {
  return {
    ...state,
    loading: false
  };
};

const setGraphData = (state, action) => {
  return {
    ...state,
    graphData: action.graphData
  };
};

const setSearchedData = (state, action) => {
  return {
    ...state,
    graphData: action.searchedGraphData,
    searchedNode: [
      action.searchedGraphData.nodes.find(node => node.name === action.nodeName)
    ],
    centeredSearchedNode: true,
    ApiFlag: action.api_flag
  };
};

const toggleCenterSearchedNode = (state, action) => {
  return {
    ...state,
    centeredSearchedNode: !state.centeredSearchedNode
  };
};

const setAPIFlag = (state, action) => {
  return {
    ...state,
    ApiFlag: action.api_flag
  };
};

const setShowSideBar = (state, action) => {
  return {
    ...state,
    showSideBar: action.is_show
  };
};

const showToast = (state, action) => {
  if (action.status === "success") {
    toast.success(action.message, {
      position: toast.POSITION.TOP_CENTER
    });
  } else if (action.status === "error") {
    toast.error(action.message, {
      position: toast.POSITION.TOP_CENTER
    });
  }
  return {
    ...state
  };
};

const setTags = (state, action) => {
  return {
    ...state,
    tags: action.tags
  };
};

const setComparedGraphData = (state, action) => {
  return {
    ...state,
    graphData: action.comparedGraphData,
    ApiFlag: "compare"
  };
};

const setSelectedTargetNode = (state, action) => {
  return {
    ...state,
    selectedTargetNodes: action.targetNodeData
  };
};

const setSelectedSourceNode = (state, action) => {
  return {
    ...state,
    selectedSourceNodes: action.sourceNodeData
  };
};

const isModalOpened = (state, action) => {
  return {
    ...state,
    modalOpened: action.is_modal_opened
  };
};

const setClickedNodeData = (state, action) => {
  return {
    ...state,
    clickedNodeDetails: action.clicked_node_data
  };
};

const setDataLoadFlag = (state, action) => {
  return {
    ...state,
    dataLoaded: action.is_loaded
  };
};

const setReferenceHandler = (state, action) => {
  return {
    ...state,
    ref: action.ref
  };
};

const onHighlightLink = (state, action) => {
  return {
    ...state,
    highlightLink: action.links
  };
};

const onHighlightNode = (state, action) => {
  return {
    ...state,
    highlightNodes: action.nodes
  };
};

const onNodeClickToLoad = (state, action) => {
  return {
    ...state,
    onNodeClickToLoad: action.flag
  };
};

const setIsZoomToLoad = (state, action) => {
  return {
    ...state,
    isZoomToLoaded: action.flag
  };
};

const setLoadedGlobalAlphaLevel = (state, action) => {
  return {
    ...state,
    loadedGlobalAlphaLevel: action.levels
  };
};

const setScreenWidth = (state, action) => {
  return {
    ...state,
    width: action.width
  };
};

const setScreenHeight = (state, action) => {
  return {
    ...state,
    height: action.height
  };
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.LOADER_START:
      return loaderStart(state, action);
    case actionType.LOADER_STOP:
      return loaderStop(state, action);
    case actionType.SET_GRAPH_DATA:
      return setGraphData(state, action);
    case actionType.SET_SEARCHED_GRAPH_DATA:
      return setSearchedData(state, action);
    case actionType.SET_COMPARED_GRAPH_DATA:
      return setComparedGraphData(state, action);
    case actionType.CENTER_SEARCHED_NODE:
      return toggleCenterSearchedNode(state, action);
    case actionType.SET_API_FLAG:
      return setAPIFlag(state, action);
    case actionType.SHOW_SIDE_BAR:
      return setShowSideBar(state, action);
    case actionType.SET_TAGS:
      return setTags(state, action);
    case actionType.SHOW_TOAST:
      return showToast(state, action);
    case actionType.SET_SELECTED_TARGET_NODE:
      return setSelectedTargetNode(state, action);
    case actionType.SET_SELECTED_SOURCE_NODE:
      return setSelectedSourceNode(state, action);
    case actionType.IS_MODAL_OPENED:
      return isModalOpened(state, action);
    case actionType.CLICKED_NODE_DETAILS:
      return setClickedNodeData(state, action);
    case actionType.DATA_LOAD_FLAG:
      return setDataLoadFlag(state, action);
    case actionType.SET_REFERENCE:
      return setReferenceHandler(state, action);
    case actionType.ON_HIGHLIGHT_LINK:
      return onHighlightLink(state, action);
    case actionType.ON_HIGHLIGHT_NODE:
      return onHighlightNode(state, action);
    case actionType.ON_NODE_CLICK_TO_LOAD:
      return onNodeClickToLoad(state, action);
    case actionType.SET_IS_ZOOM_TO_LOAD:
      return setIsZoomToLoad(state, action);
    case actionType.SET_LOADED_GLOBAL_ALPHA_LEVEL:
      return setLoadedGlobalAlphaLevel(state, action);
    case actionType.SET_SCREEN_WIDTH:
      return setScreenWidth(state, action);
    case actionType.SET_SCREEN_HEIGHT:
      return setScreenHeight(state, action);
    default:
      return state;
  }
};

export default reducer;
