import axios from '../../axios-graph';
import * as actionTypes from './actionTypes';


// import GraphData from '../../Components/Graph/Data';

export const loaderStart = () => {
    return {
        type: actionTypes.LOADER_START
    }
}

export const loaderStop = () => {
    return {
        type: actionTypes.LOADER_STOP
    }
}

export const setGraphData = (data) => {
    return {
        type: actionTypes.SET_GRAPH_DATA,
        graphData: data
    }
}

export const setSearchedGraphData = (data, name, api_flag) => {
    return {
        type: actionTypes.SET_SEARCHED_GRAPH_DATA,
        searchedGraphData: data,
        nodeName: name,
        api_flag: api_flag
    }
}

export const setComparedGraphData = (data) => {
    return {
        type: actionTypes.SET_COMPARED_GRAPH_DATA,
        comparedGraphData: data
    }
}

export const toggleCenterSearchedNode = () => {
    return {
        type: actionTypes.CENTER_SEARCHED_NODE,
    }
}

export const setAPIFlag = (flag) => {
    return {
        type: actionTypes.SET_API_FLAG,
        api_flag: flag
    }
}

export const setShowSideBar = (is_show) => {
    return {
        type: actionTypes.SHOW_SIDE_BAR,
        is_show: is_show
    }
}

export const showToast = (status, message) => {
    return {
        type: actionTypes.SHOW_TOAST,
        status: status,
        message: message
    }
}

export const setTags = (tags) => {
    return {
        type: actionTypes.SET_TAGS,
        tags: tags,
    }
}

export const setSelectedTargetNode = (targetNodeData) => {
    return {
        type: actionTypes.SET_SELECTED_TARGET_NODE,
        targetNodeData: targetNodeData,
    }
}

export const setSelectedSourceNode = (sourceNodeData) => {
    return {
        type: actionTypes.SET_SELECTED_SOURCE_NODE,
        sourceNodeData: sourceNodeData,
    }
}

export const isModalOpened = (is_modal_opened) => {
    return {
        type: actionTypes.IS_MODAL_OPENED,
        is_modal_opened: is_modal_opened,
    }
}

export const setClickedNodeData = (clicked_node_data) => {
    return {
        type: actionTypes.CLICKED_NODE_DETAILS,
        clicked_node_data: clicked_node_data,
    }
}

export const setDataLoadFlag = (is_loaded) => {
    return {
        type: actionTypes.DATA_LOAD_FLAG,
        is_loaded: is_loaded,
    }
}

export const setReferenceHandler = (ref) => {
    return {
        type: actionTypes.SET_REFERENCE,
        ref: ref,
    }
}

export const onHighlightLink = (links) => {
    return {
        type: actionTypes.ON_HIGHLIGHT_LINK,
        links: links,
    }
}

export const onHighlightNode = (nodes) => {
    return {
        type: actionTypes.ON_HIGHLIGHT_NODE,
        nodes: nodes,
    }
}

export const onNodeClickToLoad = (flag) => {
    return {
        type: actionTypes.ON_NODE_CLICK_TO_LOAD,
        flag: flag,
    }
}

export const setIsZoomToLoad = (flag) => {
    return {
        type: actionTypes.SET_IS_ZOOM_TO_LOAD,
        flag: flag,
    }
}

export const setLoadedGlobalAlphaLevel = (levels) => {
    return {
        type: actionTypes.SET_LOADED_GLOBAL_ALPHA_LEVEL,
        levels: levels,
    }
}

export const setScreenWidth = (width) => {
    return {
        type: actionTypes.SET_SCREEN_WIDTH,
        width: width,
    }
}

export const setScreenHeight = (height) => {
    return {
        type: actionTypes.SET_SCREEN_HEIGHT,
        height: height,
    }
}

export const getGraphData = () => {
    return dispatch => {
        dispatch(loaderStart());
        axios.get('/home').then(res => {
            dispatch(onNodeClickToLoad(false));
			if(res.data.nodes.length === 0 && res.data.links.length === 0){
                dispatch(showToast("error", "No Data Found!"));
			}else{
                dispatch(showToast("success", "Successfully fetch the Data!"));
            }
            dispatch(loaderStop());
            res.data.nodes.map(node => {
                let scale = parseInt(node.level) - 1;
                node.node_scale = 10 - scale;
                return node;
            })
    
            res.data.links.map(link => {
                let scale = parseInt(link.level) - 1;
                link.link_scale = 10 - scale;
                return link;
            })
            dispatch(setGraphData(res.data));
        }).catch(err => {
			console.log(err);
			dispatch(loaderStop());
        });

        // GraphData.nodes.map(node => {
        //     let scale = parseInt(node.level) - 1;
        //     node.node_scale = 10 - scale;
        //     return node;
        // })

        // GraphData.links.map(link => {
        //     let scale = parseInt(link.level) - 1;
        //     link.link_scale = 10 - scale;
        //     return link;
        // })
        // console.log(GraphData)
        // dispatch(setGraphData(GraphData));
    }
}

export const getSearchedGraphData = (name, mode, data=null, api_flag, tagLength=0) => {
    return dispatch => {
        dispatch(loaderStart());
        let param = {"name": name, "mode": mode};
        if(data && mode === 2){
            param = {"name": name, "mode": mode, "data": data};
        }
        axios.post('/search', param, {headers: {'Content-Type': 'application/json'}}).then(res => {
            dispatch(loaderStop());
            if(tagLength >= 2){
                dispatch(onNodeClickToLoad(true));
            }else{
                dispatch(onNodeClickToLoad(false));
            }
            if(res.data.nodes.length === 0 && res.data.links.length === 0){
                dispatch(showToast("error", "No Data Found!"));
            }else{
                dispatch(showToast("success", "Successfully fetch the Data!"));
                res.data.nodes.map(node => {
                    let scale = parseInt(node.level) - 1;
                    node.node_scale = 10 - scale;
                    return node;
                });
        
                res.data.links.map(link => {
                    let scale = parseInt(link.level) - 1;
                    link.link_scale = 10 - scale;
                    return link;
                });
                let max_level_id = Math.max.apply(
                    Math,
                    res.data.nodes.map(function(n) {
                      return n.level;
                    })
                )
                let min_level_id = Math.min.apply(
                    Math,
                    res.data.nodes.map(function(n) {
                      return n.level;
                    })
                )
                let scale_array = [];
                if(parseInt(max_level_id - min_level_id) > 0){
                    for(let s = 1; s < parseInt(max_level_id - min_level_id); s++){
                        scale_array.push(s);
                    }
                }
                if(scale_array.length){
                    dispatch(setLoadedGlobalAlphaLevel(scale_array));
                }
                dispatch(setSearchedGraphData(res.data, name, api_flag));
                if(mode === 2){
                    let targetNodes = [];
                    let sourceNodes = [];
                    res.data.links.filter((link) => {
                        if(link.target){
                            if(link.source.name === name){
                                let target = {};
                                target.value = link.target.id;
                                target.label = link.target.name;
                                targetNodes.push(target);
                            }
                        }
                        return link;
                    });
                    res.data.nodes.filter((n) => {
                        if(n.name !== name){
                            let source = {};
                            source.value = n.id;
                            source.label = n.name;
                            sourceNodes.push(source);
                        }
                        return n;
                    });
                    
                    dispatch(setSelectedSourceNode(sourceNodes));
                    dispatch(setSelectedTargetNode(targetNodes));
                    dispatch(setShowSideBar(true));
                }
            }
        }).catch(err => {
            // console.log(err);
            dispatch(loaderStop());
            dispatch(showToast("error", err.message));
        });
    }
}

export const getComparedGraphData = (tags) => {
    return dispatch => {
        dispatch(loaderStart());
        axios.post('/compare', {"nodes": tags, "links":[]}, 
                {headers: {'Content-Type': 'application/json'}}).then(res => {
            dispatch(loaderStop());
            dispatch(onNodeClickToLoad(true));
            if(res.data.nodes.length === 0 && res.data.links.length === 0){
                dispatch(showToast("error", "No Data Found!"));
            }else{
                dispatch(showToast("success", "Successfully fetch the Data!"));
                dispatch(setComparedGraphData(res.data));
            }
        }).catch(err => {
            // console.log(err);
            dispatch(loaderStop());
            dispatch(showToast("error", err.message));
        });
    }
}

export const zoomInToGetData = (data, level) => {
    return dispatch => {
        dispatch(loaderStart());
        axios.post('/zoom', {"data": data, "level": level}, 
                {headers: {'Content-Type': 'application/json'}}).then(res => {
            dispatch(loaderStop());
            if(res.data.nodes.length === 0 && res.data.links.length === 0){
                dispatch(showToast("error", "No Data Found!"));
            }else{
                res.data.nodes.map(node => {
                    let scale = parseInt(node.level) - 1;
                    node.node_scale = 10 - scale;
                    return node;
                })
        
                res.data.links.map(link => {
                    let scale = parseInt(link.level) - 1;
                    link.link_scale = 10 - scale;
                    return link;
                })
                dispatch(setGraphData(res.data));
                dispatch(setAPIFlag("zoom"));
                dispatch(showToast("success", "Successfully fetched the Data!"));
            }
        }).catch(err => {
            // console.log(err);
            dispatch(loaderStop());
            dispatch(showToast("error", err.message));
        });
    }
}