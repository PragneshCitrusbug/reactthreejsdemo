/* eslint-disable react/prop-types */
import React, { Component } from "react";
import { connect } from "react-redux";

import "./App.css";

import AddNode from "./Components/AddNode/AddNode";
import SideBar from "./Components/SideBar/SideBar";
import Header from "./Components/Header/Header";
import Graph from "./Components/Graph/Graph";
import Modal from "./Components/UI/Modal/Modal";
import Spinner from "./Components/UI/Spinner/Spinner";

import * as actions from "./store/actions/index";

class App extends Component {
  state = {
    addNodeform: {
      source: "",
      source_name: "",
      title: "",
      description: "",
      node_class_type: ""
    }
  };

  componentDidMount() {
    this.props.getGraphData();
    window.addEventListener("resize", this.resizeScreen);
  }

  addNewNodeHandlerOther = () => {
    if (this.state.addNodeform.title !== "") {
      // for Node
      const oldNodeData = { ...this.props.graphData };

      let node_max_id = Math.max.apply(
        Math,
        oldNodeData.nodes.map(function(o) {
          return o.id;
        })
      );
      if (node_max_id < 0) {
        node_max_id = 0;
      }

      let sourceData = oldNodeData.nodes.find(
        c => c.id === this.state.addNodeform.source
      );
      let newNode = {};
      if (sourceData) {
        newNode = {
          id: node_max_id + 1,
          name: this.state.addNodeform.title,
          group: Math.floor(Math.random() * 10),
          color: this.randomColorHandler(),
          level: sourceData.level === 1 ? 1 : sourceData.level - 1,
          visible: true,
          description: this.state.addNodeform.description,
          node_class_type: this.state.addNodeform.node_class_type,
          x: sourceData.x + Math.floor(Math.random() * 50),
          y: sourceData.y + Math.floor(Math.random() * 50),
          fx: sourceData.fx + Math.floor(Math.random() * 50),
          fy: sourceData.fy + Math.floor(Math.random() * 50)
        };
      } else {
        newNode = {
          id: node_max_id + 1,
          name: this.state.addNodeform.title,
          group: Math.floor(Math.random() * 10),
          color: this.randomColorHandler(),
          level: 1,
          visible: true,
          description: this.state.addNodeform.description,
          node_class_type: this.state.addNodeform.node_class_type
        };
      }

      oldNodeData.nodes.push(newNode);
      this.props.setGraphData(oldNodeData);

      // For Link
      if (this.state.addNodeform.source !== "") {
        const connectingNewNodeData = { ...this.props.graphData };

        let link_max_id = Math.max.apply(
          Math,
          connectingNewNodeData.links.map(function(o) {
            return o.id;
          })
        );
        if (link_max_id < 0) {
          link_max_id = 1;
        } else {
          link_max_id = link_max_id + 1;
        }

        if (node_max_id + 1 !== 1) {
          let sourceDetails = connectingNewNodeData.nodes.find(
            c => c.id === this.state.addNodeform.source
          );
          let targetDetails = connectingNewNodeData.nodes.find(
            t => t.id === node_max_id + 1
          );

          let newLink = {
            id: link_max_id,
            source: sourceDetails,
            target: targetDetails,
            value: 5,
            color: sourceDetails.color,
            visible: true,
            level: targetDetails.level
          };
          sourceDetails.level += 1;
          connectingNewNodeData.links.push(newLink);

          this.props.setGraphData(connectingNewNodeData);
        }
      }
      this.props.showToast("success", "Successfully Added the New Node!");
      this.createNewNode();
      this.props.isModalOpened(false);
    }
  };

  formChangeHandler = (event, action) => {
    const addform = this.state.addNodeform;
    if (action === "party") {
      addform.party = event.target.value;
    } else if (action === "vote") {
      addform.vote = event.target.value;
    } else if (action === "source") {
      addform.source = event.target.value;
    } else if (action === "source_name") {
      addform.source_name = event.target.value;
    }
    if (action === "title") {
      addform.title = event.target.value;
    }
    if (action === "description") {
      addform.description = event.target.value;
    }
    if (action === "node_class_type") {
      addform.node_class_type = event.target.value;
    }
    this.setState({ addNodeform: addform });
  };

  createNewNode = () => {
    const newFormState = { ...this.state.addNodeform };
    newFormState.source = "";
    newFormState.source_name = "";
    newFormState.title = "";
    newFormState.description = "";
    newFormState.node_class_type = "";
    this.props.isModalOpened(true);
    this.setState((prevState, props) => {
      return {
        addNodeform: newFormState
      };
    });
  };

  nodeClickHandler = (node, fg) => {
    this.props.setShowSideBar(true);
    this.props.setClickedNodeData(node);
    this.props.setReferenceHandler(fg);
    const distance = 40;
    const distRatio = 1 + distance / Math.hypot(node.x, node.y);
    if (distRatio !== Infinity) {
      fg.centerAt(node.x, node.y, 2000);
    }

    // SET Source Node And Target Node
    let targetNodes = [];
    let sourceNodes = [];
    this.props.graphData.links.filter(link => {
      if (link.target) {
        if (link.source.id === node.id) {
          let target = {};
          target.value = link.target.id;
          target.label = link.target.name;
          targetNodes.push(target);
        }
      }
      return link;
    });
    this.props.graphData.nodes.filter(n => {
      if (n.id !== node.id) {
        let source = {};
        source.value = n.id;
        source.label = n.name;
        sourceNodes.push(source);
      }
      return n;
    });

    this.props.setSelectedTargetNode(targetNodes);
    this.props.setSelectedSourceNode(sourceNodes);
    if (this.props.clickToLoad) {
      if (this.props.showSideBar) {
        this.props.setShowSideBar(false);
      }
      this.props.getSearchedGraphData(
        node.name,
        2,
        this.props.graphData,
        "click-search",
        this.props.tags.length
      );
    }
  };

  randomColorHandler = () => {
    let letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  referenceHandler = el => {
    if (this.props.graphData.nodes.length) {
      if (el && !this.props.dataLoaded) {
        this.props.setDataLoadFlag(true);
        el.zoom(0.6, 1000);
        el.centerAt(
          this.props.graphData.nodes[0].x,
          this.props.graphData.nodes[0].y,
          2000
        );
      }
      if (this.props.ApiFlag === "compare") {
        el.zoom(2, 1000);
        el.centerAt(
          this.props.graphData.nodes[0].x,
          this.props.graphData.nodes[0].y,
          2000
        );
        this.props.setAPIFlag("");
      }
      if (this.props.ApiFlag === "zoom") {
        let zoomArray = [27, 17, 9.8, 6.3, 4.5, 3.5, 2.3, 1.6, 1.1];
        let minLevel = Math.min.apply(
          Math,
          this.props.graphData.nodes.map(function(n) {
            return n.level;
          })
        );
        el.zoom(parseFloat(zoomArray[minLevel - 1]), 1000);
        el.centerAt(
          this.props.graphData.nodes[0].x,
          this.props.graphData.nodes[0].y,
          2000
        );
        this.props.setAPIFlag("");
      }
    }
    return el;
  };

  handleLinkHoverHandler = link => {
    let links = [];
    let source = this.props.graphData.nodes.find(source =>
      link ? source.id === link.source.id : null
    );
    if (source) {
      links = this.props.graphData.links.filter(link => {
        return link.source.id === source.id;
      });
    }
    this.props.onHighlightLink(links);
  };

  handleNodeHover = node => {
    this.props.onHighlightNode(node ? [node] : []);
    let links = this.props.graphData.links.filter(link =>
      node ? link.source.id === node.id : null
    );
    this.props.onHighlightLink(links);
  };

  onSearch = e => {
    e.preventDefault();
    this.props.setAPIFlag("");
    if (this.props.tags.length) {
      if (this.props.showSideBar) {
        this.props.setShowSideBar(false);
      }
      if (this.props.tags.length === 1) {
        // For Search
        this.props.getSearchedGraphData(
          this.props.tags.join(","),
          1,
          null,
          "search"
        );
      } else {
        // For Compare
        this.props.getComparedGraphData(this.props.tags);
      }
    } else {
      this.props.showToast("error", "Please Insert Search or Compare tags!");
    }
  };

  centerSearchHandler = () => {
    this.props.toggleCenterSearchedNode();
  };

  linkVisibilityHandler = link => {
    return true;
  };

  closeSideBarHandler = () => {
    this.props.setShowSideBar(false);
  };

  onNodeDragEndHandler = node => {
    const existNodeData = { ...this.props.graphData };
    existNodeData.links.forEach(link => {
      let source = existNodeData.nodes.find(res => res.id === link.source.id);
      let target = existNodeData.nodes.find(res => res.id === link.target.id);
      link.source = source;
      link.target = target;
    });
    this.props.setGraphData(existNodeData);
  };

  onNodeDragHandler = node => {
    const existNodeData = { ...this.props.graphData };
    existNodeData.links.forEach(link => {
      let source = existNodeData.nodes.find(res => res.id === link.source.id);
      let target = existNodeData.nodes.find(res => res.id === link.target.id);
      link.source = source;
      link.target = target;
    });
    this.props.setGraphData(existNodeData);
  };

  modalCancelHandler = () => {
    this.props.isModalOpened(false);
  };

  addSubNodeHandler = node => {
    this.props.setShowSideBar(true);
    this.props.setClickedNodeData(node);
    this.createNewNode();
    const newNodeClickState = { ...this.state.addNodeform };

    newNodeClickState.source_name = node.name;
    newNodeClickState.source = node.id;
    newNodeClickState.title = "";
    newNodeClickState.description = "";
    newNodeClickState.node_class_type = "";
    this.setState((prevState, props) => {
      return {
        addNodeform: newNodeClickState
      };
    });

    this.addNewNodeHandlerOther(node);

    const distance = 40;
    const distRatio = 1 + distance / Math.hypot(node.x, node.y);
    if (distRatio !== Infinity) {
      this.props.refObj.centerAt(node.x, node.y, 2000);
    }
  };

  targetChangedHandler = (event, sourceId) => {
    this.props.setSelectedTargetNode(event);

    const node_data = { ...this.props.graphData };
    let source_node = node_data.nodes.find(n => n.id === sourceId);
    let links = node_data.links.filter(link => link.source.id === sourceId);
    let list_of_link_ids = [];
    event.map(obj => {
      return list_of_link_ids.push(obj.value);
    });

    let remove_link = links.filter(
      link => list_of_link_ids.indexOf(link.target.id) === -1
    );

    if (remove_link.length) {
      let updatedLinks = node_data.links.filter(
        link => remove_link.indexOf(link) === -1
      );
      node_data.links = updatedLinks;
      this.props.showToast("success", "Successfully Removed the Link!");
    } else {
      for (let l = 0; l < list_of_link_ids.length; l++) {
        let existLink = node_data.links.find(
          link =>
            link.target.id === list_of_link_ids[l] &&
            link.source.id === sourceId
        );
        if (!existLink) {
          let link_max_id = Math.max.apply(
            Math,
            node_data.links.map(function(o) {
              return o.id;
            })
          );
          if (link_max_id < 0) {
            link_max_id = 1;
          } else {
            link_max_id = link_max_id + 1;
          }
          let targetDetails = node_data.nodes.find(
            t => t.id === list_of_link_ids[l]
          );
          let newLink = {
            id: link_max_id,
            source: source_node,
            target: targetDetails,
            value: 5,
            color: source_node.color,
            level: targetDetails.level
          };
          node_data.links.push(newLink);
        }
      }
      this.props.showToast("success", "Successfully Added the Link!");
    }
    this.props.setGraphData(node_data);
  };

  handleChange(tags) {
    this.props.setTags(tags);
    return tags;
  }

  zoomInToGetNode(flag, alpha_level, level) {
    // console.log(flag);
    // console.log(alpha_level);
    this.props.setIsZoomToLoad(flag);
    let level_list = [...this.props.loadedGlobalAlphaLevel];
    // console.log(level_list);
    level_list.push(alpha_level);
    this.props.setLoadedGlobalAlphaLevel(level_list);
    // console.log(this.props.graphData);
    // let level = 3;
    this.props.zoomInToGetData(this.props.graphData, level);
  }

  resizeScreen = () => {
    this.props.setScreenWidth(window.innerWidth);
    this.props.setScreenHeight(window.innerHeight);
    // width = window.innerWidth, height = window.innerHeight;
  }

  render() {
    let loadGraph = <Spinner />;
    if (!this.props.loading) {
      loadGraph = (
        <Graph
          data={this.props.graphData}
          clicked={this.nodeClickHandler}
          linkHover={this.handleLinkHoverHandler}
          highlightLink={this.props.highlightLink}
          highlightNodes={this.props.highlightNodes}
          nodeHoverHandler={this.handleNodeHover}
          linkVisibilityHandler={this.linkVisibilityHandler}
          referenceHandler={this.referenceHandler}
          searchedNode={this.props.searchedNode}
          centeredSearchedNode={this.props.centeredSearchedNode}
          centerSearchHandler={this.centerSearchHandler}
          onNodeDragEndHandler={this.onNodeDragEndHandler}
          onNodeDragHandler={this.onNodeDragHandler}
          maxNodeZoomLevel={Math.max.apply(
            Math,
            this.props.graphData.nodes.map(function(n) {
              return n.level;
            })
          )}
          minNodeZoomLevel={
            Math.min.apply(
              Math,
              this.props.graphData.nodes.map(function(n) {
                return n.level;
              })
            )}
          maxLinkZoomLevel={Math.max.apply(
            Math,
            this.props.graphData.links.map(function(l) {
              return l.level;
            })
          )}
          ApiFlag={this.props.ApiFlag}
          zoomInToGetNode={(flag, alpha_level, level) =>
            this.zoomInToGetNode(flag, alpha_level, level)
          }
          isZoomToLoaded={this.props.isZoomToLoaded}
          loadedGlobalAlphaLevel={this.props.loadedGlobalAlphaLevel}
        />
      );
    }
    return (
      <div>
        <Header
          addNodeClicked={this.createNewNode}
          changed={tag => this.handleChange(tag)}
          tags={this.props.tags}
          onSearchClicked={this.onSearch}
        />
        <div className="main">
          <div className="graph">
            <div className="container">
              <Modal
                show={this.props.modalOpened}
                modalClosed={this.modalCancelHandler}
              >
                <AddNode
                  source_name={this.state.addNodeform.source_name}
                  source={this.state.addNodeform.source}
                  title={this.state.addNodeform.title}
                  description={this.state.addNodeform.description}
                  node_class_type={this.state.addNodeform.node_class_type}
                  addNewNode={this.addNewNodeHandlerOther}
                  changed={this.formChangeHandler}
                />
              </Modal>
            </div>
            <div style={{ position: "absolute" }}>{loadGraph}</div>
          </div>
          <div className="side-bar">
            {this.props.showSideBar ? (
              <SideBar
                onClose={this.closeSideBarHandler}
                title="FOCUS MODE"
                nodeData={this.props.clickedNodeDetails}
                addSubNode={this.addSubNodeHandler}
                // refHandler={this.props.ref}
                targetChanged={this.targetChangedHandler}
                graphData={this.props.graphData}
                sourceData={this.props.selectedSourceNodes}
                selectedTargetNodes={this.props.selectedTargetNodes}
              />
            ) : null}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    graphData: state.graphData,
    loading: state.loading,
    searchedNode: state.searchedNode,
    centeredSearchedNode: state.centeredSearchedNode,
    ApiFlag: state.ApiFlag,
    showSideBar: state.showSideBar,
    tags: state.tags,
    selectedTargetNodes: state.selectedTargetNodes,
    selectedSourceNodes: state.selectedSourceNodes,
    modalOpened: state.modalOpened,
    clickedNodeDetails: state.clickedNodeDetails,
    dataLoaded: state.dataLoaded,
    refObj: state.ref,
    highlightLink: state.highlightLink,
    highlightNodes: state.highlightNodes,
    clickToLoad: state.onNodeClickToLoad,
    isZoomToLoaded: state.isZoomToLoaded,
    loadedGlobalAlphaLevel: state.loadedGlobalAlphaLevel,
    width: state.width,
    height: state.height
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getGraphData: () => dispatch(actions.getGraphData()),
    getSearchedGraphData: (name, mode, data = null, api_flag, tagLength = 0) =>
      dispatch(
        actions.getSearchedGraphData(name, mode, data, api_flag, tagLength)
      ),
    toggleCenterSearchedNode: () =>
      dispatch(actions.toggleCenterSearchedNode()),
    setAPIFlag: flag => dispatch(actions.setAPIFlag(flag)),
    setShowSideBar: is_show => dispatch(actions.setShowSideBar(is_show)),
    setTags: tags => dispatch(actions.setTags(tags)),
    showToast: (status, message) =>
      dispatch(actions.showToast(status, message)),
    getComparedGraphData: tags => dispatch(actions.getComparedGraphData(tags)),
    setSelectedTargetNode: targetNodeData =>
      dispatch(actions.setSelectedTargetNode(targetNodeData)),
    setGraphData: data => dispatch(actions.setGraphData(data)),
    setSelectedSourceNode: sourceNodeData =>
      dispatch(actions.setSelectedSourceNode(sourceNodeData)),
    isModalOpened: is_opened => dispatch(actions.isModalOpened(is_opened)),
    setClickedNodeData: clicked_node_data =>
      dispatch(actions.setClickedNodeData(clicked_node_data)),
    setDataLoadFlag: is_loaded => dispatch(actions.setDataLoadFlag(is_loaded)),
    setReferenceHandler: ref => dispatch(actions.setReferenceHandler(ref)),
    onHighlightLink: link => dispatch(actions.onHighlightLink(link)),
    onHighlightNode: node => dispatch(actions.onHighlightNode(node)),
    onNodeClickToLoad: flag => dispatch(actions.onNodeClickToLoad(flag)),
    setIsZoomToLoad: flag => dispatch(actions.setIsZoomToLoad(flag)),
    zoomInToGetData: (data, level) =>
      dispatch(actions.zoomInToGetData(data, level)),
    setLoadedGlobalAlphaLevel: levels =>
      dispatch(actions.setLoadedGlobalAlphaLevel(levels)),
    setScreenWidth: width => dispatch(actions.setScreenWidth(width)),
    setScreenHeight: height => dispatch(actions.setScreenHeight(height))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
