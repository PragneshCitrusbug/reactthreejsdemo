import React, { Component } from "react";
import Auxiliary from "../../hoc/Auxiliary";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";

import "./Header.css";

class Header extends Component {
  render() {
    const props = this.props;
    return (
      <Auxiliary>
        <nav
          className="navbar navbar-light"
          style={{ backgroundColor: "#e3f2fd" }}
        >
          <a className="navbar-brand" href="/">
            React Three JS
          </a>
          <div style={{ marginLeft: "auto" }}>
            <button
              className="btn btn-outline-success my-2 my-sm-0 h-42"
              onClick={props.addNodeClicked}
            >
              <i className="fa fa-plus wi-25" aria-hidden="true" />
            </button>
          </div>
          &nbsp;&nbsp;
          <div className="form-inline">
            <TagsInput
              className="form-control mr-sm-2 h-45"
              inputProps={{ placeholder: "Search or Compare" }}
              value={props.tags}
              onChange={tag => props.changed(tag)}
            />
            <button
              className="btn btn-outline-primary my-2 my-sm-0 h-42"
              onClick={props.onSearchClicked}
            >
              <i className="fa fa-search" aria-hidden="true" />
            </button>
          </div>
        </nav>
      </Auxiliary>
    );
  }
}

export default Header;
