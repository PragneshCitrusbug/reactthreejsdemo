import React, {Component} from 'react';
import './AddNode.css';
import Auxiliary from '../../hoc/Auxiliary';

class AddNode extends Component {

    render(){
        const props = this.props;
        return (
            <Auxiliary>
                <div>
                    <div className="form-row">
                        <div className="form-group col-md-12">
                            <label className="f-l">Source</label>
                            <input type="text" className="form-control" id="source" name="source_name" 
                                value={props.source_name} 
                                onChange={(event) => props.changed(event, 'source_name')} disabled/>
                        </div>
                        <div className="form-group col-md-12">
                            <input type="text" className="form-control" id="sourceName" name="sourceName" 
                                value={props.source} 
                                onChange={(event) => props.changed(event, 'source')} hidden/>
                        </div>
                        <div className="form-group col-md-12">
                            <label className="f-l">TITLE</label>
                            <input type="text" className="form-control" id="title" name="title" 
                                value={props.title} 
                                onChange={(event) => props.changed(event, 'title')}/>
                        </div>
                        <div className="form-group col-md-12">
                            <label className="f-l">DESCRIPTION</label>
                            <input type="text" className="form-control" id="description" name="description" 
                                value={props.description} 
                                onChange={(event) => props.changed(event, 'description')}/>
                        </div>
                        <div className="form-group col-md-12">
                            <label className="f-l">CLASS</label>
                            <input type="text" className="form-control" id="node_class_type" name="node_class_type" 
                                value={props.node_class_type} 
                                onChange={(event) => props.changed(event, 'node_class_type')}/>
                        </div>
                    </div>
                    <button onClick={() => props.addNewNode()} className="btn btn-success">ADD</button>
                </div>
            </Auxiliary>
        );
    }
}

export default AddNode;