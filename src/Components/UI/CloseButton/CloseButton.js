import React, { Component } from 'react';
// import PropTypes from 'prop-types';

import './CloseButton.css';

class CloseButton extends Component {
  render() {
	const props = this.props;
    return(
      <button className="close-button" onClick={props.onClick} type="button">
          {props.children}
      </button>
    );
  }
}
// const CloseButton = (props) => (

// );

// CloseButton.propTypes = {
// //   children: PropTypes.oneOfType([PropTypes.string, PropTypes.array, PropTypes.node]).isRequired,
// //   onClick: PropTypes.func.isRequired,
// };

export default CloseButton;
