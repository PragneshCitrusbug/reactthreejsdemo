import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Modal.css';

import Auxiliary from '../../../hoc/Auxiliary';
import Backdrop from '../BackDrop/BackDrop';

class Modal extends Component {
    shouldComponentUpdate(nextProps, nextState){
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }
    render () {
        const props = this.props;
        return (
            <Auxiliary>
                <Backdrop show={props.show} clicked={props.modalClosed}/>
                <div className="Modal"
                    style ={{
                        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                        opacity: props.show ? '1' : '0'
                    }}
                >
                    {props.children}
                </div>
            </Auxiliary>
        );
    }
};

Modal.propTypes = {
    show: PropTypes.any,
    children: PropTypes.any  
};

export default Modal;