import React, { Component } from 'react';
import './BackDrop.css'

class backDrop extends Component {
    render(){
        const props = this.props;
        return (
            props.show ? <div className="Backdrop" onClick={props.clicked}></div> : null    
        )
    }
}
// const backDrop = (props) => (
    
// );

export default backDrop;
