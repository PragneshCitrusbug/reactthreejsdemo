import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import './SideBar.css';
import Select from 'react-select';

import CloseButton from '../UI/CloseButton/CloseButton';
import closeIcon from '../../assets/svg/close-icon-grey.svg';

class SideBar extends Component {

    render() {
        const props = this.props;
        return (
            <div className="sideBar">

                <CloseButton onClick={props.onClose}>
                    <img src={closeIcon} alt="close" />
                </CloseButton>
                <h2 className="side-bar-title">{props.title}</h2><br /><hr />
                <div className="button-center">

                    <button className="btn btn-success" 
                        onClick={() => props.addSubNode(props.nodeData)}>
                        ADD NODE UNDER THIS NODE
                    </button>
                    <br /><hr />
                </div>
                <div>
                    <h6>SELECT TARGET NODE</h6>
                    <Select
                        options={props.sourceData}
                        value={props.selectedTargetNodes}
                        onChange={(event) => props.targetChanged(event, props.nodeData.id)}
                        isMulti
                    />
                    <hr />
                </div>

                <h4>ID</h4>
                {props.nodeData.id}

                <br /><hr />

                <h4>TITLE</h4>
                {props.nodeData.name}

                <br /><hr />

                <h4>DESCRIPTION</h4>
                {props.nodeData.description}

                <br /><hr />

                <h4>CLASS</h4>
                {props.nodeData.node_class_type}

                <br /><hr />

                <h4>Zoom Level</h4>
                {props.nodeData.level}

            </div>
        );
    };
};

export default SideBar;
