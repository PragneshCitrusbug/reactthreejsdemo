/* eslint-disable react/prop-types */
import React, { Component } from "react";
import "./Graph.css";
import { ForceGraph2D } from "react-force-graph";
import * as d3Force from "d3-force-3d";
// import $ from 'jquery';
// import * as d3 from "d3";
// import * as THREE from 'three';
// import SpriteText from 'three-spritetext';

class Graph extends Component {
  componentDidMount() {
    this.fg.d3Force(
      "link",
      d3Force
        .forceLink()
        .id(d => {
          let nodes = this.props.data.nodes;
          this.props.data.links.forEach(function(link) {
            let source = nodes.find(res => res.id === link.source.id);
            let target = nodes.find(res => {
              if (link.target) {
                if (res.id === link.target.id) {
                  return res;
                }
              }
              // eslint-disable-next-line array-callback-return
              return;
            });
            if (target) {
              link.source = source;
              link.target = target;
            }
          });
          return d.index;
        })
        .distance(200)
        .strength(5)
    );
  }

  calcNodeLevel(global_scale, level, max_level, node_scale) {
    // let scale_array = [0.5, 1, 1.5, 2.3, 2.5, 3, 3.5, 4, 4.5, 5];
    let scale_array = [0.5, 1, 1.5, 2.3, 3, 4.5, 6.3, 9.6, 16.7, 22];
    let divisable = 5;
    for (let i = 0; i < 10; i++) {
      if (global_scale >= scale_array[i] && level === max_level - i) {
        // console.log(global_scale);
        return { status: true, divisable: divisable };
      }
    }
  }

  calcNodeGlobalAlpha(level, max_level, scale, divisable, node_scale) {
    let decrement = 0;
    if (level === 10) {
      decrement = 0;
    } else if (level === 9) {
      decrement = 0.9;
    } else if (level === 8) {
      decrement = 1.2;
    } else if (level === 7) {
      decrement = 1.5;
    } else if (level === 6) {
      decrement = 1.8;
    } else if (level === 5) {
      decrement = 2.1;
      // node_scale = 9;
    } else if (level === 4) {
      decrement = 2.4;
      node_scale = 9;
    } else if (level === 3) {
      decrement = 2.7;
      node_scale = 14;
    } else if (level === 2) {
      decrement = 3.3;
      node_scale = 20;
    } else if (level === 1) {
      decrement = 3.6;
      node_scale = 26;
    }
    // console.log(scale)
    let global_alpha = (scale * level) / 10;

    for (let i = 0; i < 10; i++) {
      if (level === max_level - i) {
        if (scale >= parseInt(node_scale)) {
          global_alpha = 1;
        } else {
          if (scale >= parseInt(node_scale) - 1) {
          }
          if (scale < parseInt(node_scale)) {
            global_alpha = global_alpha - decrement;
          }
        }

        return { global_alpha: global_alpha };
      }
    }
  }

  calcLinkLevel(global_scale, level, max_level) {
    let scale_array = [1, 1.5, 2.3, 3, 4.5, 6.3, 9.6, 16.7, 22];
    let divisable = 5;
    for (let i = 0; i < 10; i++) {
      if (global_scale >= scale_array[i] && level === max_level - i) {
        return { status: true, divisable: divisable };
      }
    }
  }

  calcLinkGlobalAlpha(level, max_level, scale, divisable, link_scale) {
    // if(max_level >= 4){
    //     global_alpha = global_alpha - (decrement * (max_level - 3));
    // }
    let decrement = 0;
    if (level === 10) {
      decrement = 0;
    } else if (level === 9) {
      decrement = 0.9;
    } else if (level === 8) {
      decrement = 1.2;
    } else if (level === 7) {
      decrement = 1.5;
    } else if (level === 6) {
      decrement = 1.8;
    } else if (level === 5) {
      decrement = 2.1;
    } else if (level === 4) {
      decrement = 2.4;
      link_scale = 9;
    } else if (level === 3) {
      decrement = 2.7;
      link_scale = 14;
    } else if (level === 2) {
      decrement = 3.3;
      link_scale = 20;
    } else if (level === 1) {
      decrement = 3.6;
      link_scale = 26;
    }
    // console.log(max_level)
    let global_alpha = (scale * level) / 10;

    for (let i = 0; i < 10; i++) {
      if (level === max_level - i) {
        if (scale >= parseInt(link_scale)) {
          global_alpha = 1;
        } else {
          if (scale >= parseInt(link_scale) - 1) {
          }
          if (scale < parseInt(link_scale)) {
            global_alpha = global_alpha - decrement;
          }
        }
        return { global_alpha: global_alpha };
      }
    }
    // return {"global_alpha": global_alpha }
  }

  render() {
    // const GROUPS = 12;
    return (
      <div className="graph">
        <ForceGraph2D
          ref={el => {
            if (el) {
              this.fg = el;
            }
            if (this.props.data && this.fg) {
              if (this.props.data.nodes.length === 1) {
                this.fg.zoom(0.5, 1000);
                this.fg.centerAt(
                  this.props.data.nodes[0].x,
                  this.props.data.nodes[0].y,
                  2000
                );
              }
              this.props.referenceHandler(this.fg);
            }
            if (this.props.data && this.fg) {
              this.props.data.nodes.map(node => {
                if (
                  this.props.searchedNode.indexOf(node) !== -1 &&
                  this.props.centeredSearchedNode
                ) {
                  // console.log(this.props.ApiFlag);
                  if (this.props.ApiFlag === "click-search") {
                    this.fg.zoom(1, 1000);
                  } else {
                    this.fg.zoom(0.5, 1000);
                  }
                  this.fg.centerAt(node.x, node.y, 2000);
                  this.props.centerSearchHandler();
                }
                return node;
              });
            }
          }}
          height={window.innerHeight}
          width={window.innerWidth}
          graphData={this.props.data}
          backgroundColor="#ffffff"
          showNavInfo={false}
          nodeLabel={d => {
            return d.name;
          }}
          // dagMode="td" // Options : td, bu, lr, rl, radialout, radialin, null
          // dagLevelDistance={30}
          //nodeAutoColorBy="group"

          // nodeAutoColorBy={d => d.id%GROUPS}
          // linkAutoColorBy={d => this.props.data.nodes[d.id].id%GROUPS}

          linkDirectionalParticles="value"
          linkDirectionalParticleSpeed={d => d.value * 0.001}
          linkDirectionalArrowRelPos={1}
          linkDirectionalArrowColor={link => {
            return link.color;
          }}
          // linkCurvature={0.5}
          // enableNodeDrag = {false}

          linkDirectionalParticleColor="#ff00ff"
          d3VelocityDecay={1}
          d3AlphaDecay={1}
          onNodeClick={node => this.props.clicked(node, this.fg)}
          nodeCanvasObject={async (node, ctx, globalScale) => {
            let scale = Math.round(globalScale);

            // const label = node.name;
            const fontSize = 18 / scale;
            ctx.font = `${fontSize}px Sans-Serif`;
            ctx.fillStyle = "rgba(255, 255, 255, 0.5)";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.fillStyle = node.color;

            if (this.props.highlightNodes.indexOf(node) !== -1) {
              ctx.fillStyle = "red";
            }
            if (
              this.props.searchedNode.indexOf(node) !== -1 &&
              this.props.centeredSearchedNode
            ) {
              // this.fg.centerAt(node.x, node.y, 2000);
              this.props.centerSearchHandler();
            }
            // if(node.visible){

            // ctx.fillText(node.name + "( " + node.x + ", " + node.y +" )", node.x, node.y+30/scale);
            // ctx.fillText(node.name, node.x, node.y+30/scale);
            // console.log(parseFloat(globalScale.toFixed(2)), node.node_scale);
            // console.log(this.props.minNodeZoomLevel);
            // console.log(node.node_scale);
            let scale_array = [0.5, 1, 1.5, 2.3, 3, 4.5, 6.3, 9.6, 16.7, 22];
             if(parseFloat(globalScale.toFixed(2)) > node.node_scale && 
                parseFloat(globalScale.toFixed(2)) >= scale_array[node.node_scale]){
                 if(this.props.loadedGlobalAlphaLevel.indexOf(node.node_scale) === -1){
                     this.props.zoomInToGetNode(false, node.node_scale, this.props.minNodeZoomLevel);
                 }
             }
            //  if(parseFloat(globalScale.toFixed(2)) > 1 && parseFloat(globalScale.toFixed(2)) <= 1.15){
            //      if(this.props.loadedGlobalAlphaLevel.indexOf(1.15) === -1){
            //          //this.props.zoomInToGetNode(false, parseFloat(globalScale.toFixed(2)), 
            //          // this.props.maxNodeZoomLevel);
            //      }
            //  }else if(parseFloat(globalScale.toFixed(2)) > 1.52 && parseFloat(globalScale.toFixed(2)) <= 1.74){
            //      if(this.props.loadedGlobalAlphaLevel.indexOf(1.74) === -1){
            //          //this.props.zoomInToGetNode(false, parseFloat(globalScale.toFixed(2)), 
            //          // this.props.maxNodeZoomLevel - 1);
            //      }
            //  }
            let visiblePoint = false;

            let divisable = 5;
            let result = this.calcNodeLevel(
              parseFloat(globalScale.toFixed(2)),
              node.level,
              this.props.maxNodeZoomLevel,
              node.node_scale
            );
            if (result) {
              visiblePoint = true;
              divisable = result.divisable;
            } else {
              visiblePoint = false;
            }
            node.visible = false;
            let global_alpha;
            let ga_result = this.calcNodeGlobalAlpha(
              node.level,
              this.props.maxNodeZoomLevel,
              parseFloat(globalScale.toFixed(2)),
              divisable,
              node.node_scale
            );
            if (ga_result) {
              global_alpha = ga_result["global_alpha"];
              // visiblePoint = ga_result['status']
            }
            // let decrement = 0.2;
            // let global_alpha = scale * node.level / divisable;
            // if (this.props.maxNodeZoomLevel === 10) {
            //     global_alpha = global_alpha - decrement
            // }
            // else if (this.props.maxNodeZoomLevel === 5) {
            //     global_alpha = global_alpha - (decrement * 2)
            // }
            // console.log(global_alpha)
            if (visiblePoint) {
              node.visible = true;
              ctx.beginPath();
              ctx.arc(node.x, node.y, 12 / scale, 0, 2 * Math.PI, false);
              ctx.globalAlpha = global_alpha;
              ctx.fill();
              ctx.fillText(
                node.name + "( " + node.x + ", " + node.y + " )",
                node.x,
                node.y + 30 / scale
              );
            }
          }}
          linkWidth={link => {
            const matcheLink = this.props.highlightLink.find(
              matchlink => matchlink.id === link.id
            );
            if (matcheLink) {
              return 6;
            }
            return 1;
          }}
          linkDirectionalParticleWidth={link => {
            const matcheLink = this.props.highlightLink.find(
              matchlink => matchlink.id === link.id
            );
            if (matcheLink && link.visible) {
              return 8;
            }
            return 0;
          }}
          linkCanvasObject={(link, context, global_scale) => {
            context.beginPath();
            let scale = Math.round(global_scale);
            context.strokeStyle = link.color;
            context.lineWidth = 1;
            if (this.props.highlightLink.indexOf(link) !== -1) {
              context.lineWidth = 2;
            }
            let visiblePoint = false;
            link.visible = false;
            let divisable = 5;
            let result = this.calcLinkLevel(
              parseFloat(global_scale.toFixed(2)),
              link.level,
              this.props.maxLinkZoomLevel,
              scale
            );
            // let global_alpha = scale * link.level / divisable;
            if (result) {
              visiblePoint = true;
              divisable = result.divisable;
            } else {
              visiblePoint = false;
            }

            let global_alpha;
            let ga_result = this.calcLinkGlobalAlpha(
              link.level,
              this.props.maxLinkZoomLevel,
              parseFloat(global_scale.toFixed(2)),
              divisable,
              link.link_scale
            );
            if (ga_result) {
              global_alpha = ga_result["global_alpha"];
            }

            // let decrement = 0.2;
            // let global_alpha = scale * link.level / divisable;
            // if (this.props.maxNodeZoomLevel === 4) {
            //     global_alpha = global_alpha - decrement
            // } else if (this.props.maxNodeZoomLevel === 5) {
            //     global_alpha = global_alpha - (decrement * 2)
            // }

            // scale*link.level/5>0.5
            if (visiblePoint) {
              link.visible = true;
              context.globalAlpha = global_alpha;
              context.moveTo(link.source.fx, link.source.fy);
              context.lineTo(link.target.fx, link.target.fy);
            }
            context.stroke();
            context.closePath();
          }}
          linkVisibility={link => this.props.linkVisibilityHandler(link)}
          onNodeHover={node => this.props.nodeHoverHandler(node)}
          onLinkHover={link => this.props.linkHover(link)}
          onNodeDrag={node => this.props.onNodeDragHandler(node)}
          onNodeDragEnd={node => this.props.onNodeDragEndHandler(node)}
          linkDirectionalArrowLength={link => {
            const matcheLink = this.props.highlightLink.find(
              matchlink => matchlink.id === link.id
            );
            if (matcheLink && link.visible) {
              return 4;
            }
            return 0;
          }}
          onNodeRightClick={node => this.props.onNodeRightClickHandler(node)}
        />
      </div>
    );
  }
}

export default Graph;
